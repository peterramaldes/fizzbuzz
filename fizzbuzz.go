package main

import (
	"fmt"
	"strconv"
)

func main() {
	fmt.Print("Enter an integer number: ")
	var input int
	fmt.Scanf("%d", &input)

	for i := 1; i <= input; i++ {
		fmt.Println(fizzbuzz(i))
	}
}

func fizzbuzz(i int) string {
	var result string

	if i%3 == 0 && i%5 == 0 {
		result = "FizzBuzz"
	} else if i%3 == 0 {
		result = "Fizz"
	} else if i%5 == 0 {
		result = "Buzz"
	} else {
		result = strconv.Itoa(i)
	}

	return result
}
